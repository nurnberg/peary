/**
 * Caribou Device API class header
 */

#ifndef CARIBOU_MIDDLEWARE_H
#define CARIBOU_MIDDLEWARE_H

#include "configuration.hpp"
#include "constants.hpp"
#include "device.hpp"
#include "dictionary.hpp"

#include <stdint.h>
#include <string>
#include <vector>

#include "pearydevice.hcc"
#include "pearydevice.tcc"

#endif /* CARIBOU_MIDDLEWARE_H */
